# -*- coding: utf-8 -*-
#/#############################################################################
#
#    BizzAppDev
#    Copyright (C) 2004-TODAY bizzappdev(<http://www.bizzappdev.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#/#############################################################################


from openerp.osv import osv, fields
from openerp.tools import config


class project_configuration(osv.TransientModel):
    _inherit = 'base.config.settings'

    _columns = {
        'maintenance': fields.boolean('Active Maintenance Mode'),
    }

    def get_default_maintenance(self, cr, uid, ids, context=None):

        maintenance = config.get('maintenance', {}).get(cr.dbname, False)

        return {'maintenance': maintenance or False}

    def set_maintenance(self, cr, uid, ids, context=None):
        if not config.get('maintenance'):
            config['maintenance'] = {}

        config_parameters = self.pool.get("ir.config_parameter")
        for record in self.browse(cr, uid, ids, context=context):
            config['maintenance'][cr.dbname] = record.maintenance
            config_parameters.set_param(
                cr, uid, "maintenance", record.maintenance or '',
                context=context)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
