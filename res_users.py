# -*- coding: utf-8 -*-
#/#############################################################################
#
#    BizzAppDev
#    Copyright (C) 2004-TODAY bizzappdev(<http://www.bizzappdev.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#/#############################################################################

from openerp.osv import osv
from openerp.tools import config
from openerp import SUPERUSER_ID
from openerp import pooler
from openerp.tools.translate import _


class res_users(osv.osv):
    _inherit = 'res.users'

    def __set_from_config(self, db):

        if not config.get('maintenance'):
            config['maintenance'] = {}

        if db not in config['maintenance']:
            cr = pooler.get_db(db).cursor()
            ir_config_val = self.pool.get("ir.config_parameter").get_param(
                cr, SUPERUSER_ID, "maintenance", default=None)
            config['maintenance'][db] = ir_config_val

    def check(self, db, uid, passwd):
        self.__set_from_config(db)
        if config['maintenance'][db] and uid != SUPERUSER_ID:
            raise osv.except_osv(_('Under Maintenance!'),
                                 _("System is under Maintenance"))
        return super(res_users, self).check(db, uid, passwd)

    def authenticate(self, db, login, password, user_agent_env):
        self.__set_from_config(db)
        uid = super(res_users, self).authenticate(db, login, password,
                                                  user_agent_env)
        if config['maintenance'][db] and uid != SUPERUSER_ID:
            raise osv.except_osv(_('Under Maintenance!'),
                                 _("System is under Maintenance"))
        return uid

res_users()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
