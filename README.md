OpenERP - Maintenance
=====================

OpenERP / Odoo Module which help you to put your OpenERP under Maintenance mode and also allow user to avoid loss of data as well as avoid too much communications.

Features:
---------

    * Enable Maintenance for specific database.
    * Login screen message.
    * Only SuperAdmin can login.
    * Already logged in user will get Message.


Usage Of the Module:
====================

Install `maintence_mode` module:
--------------------------------

![install.png](https://bitbucket.org/repo/GRqnk7/images/2235466654-install.png)

Activate Maintenance Mode:
--------------------------

Settings -> Configuration -> General Settings -> Options -> **Active Maintenance Mode**.
Then Click on ``Apply``

![active_true.png](https://bitbucket.org/repo/GRqnk7/images/2779817333-active_true.png)


* After activating the Maintenance mode it will not allow to login with any other user except SuperAdmin. 

![Login_New.png](https://bitbucket.org/repo/GRqnk7/images/2147774188-Login_New.png)


* If any other user tried to login in database the system will generate the message as shown in following screenshot.

![demo_db_with_warning.png](https://bitbucket.org/repo/GRqnk7/images/2678280329-demo_db_with_warning.png)

* SuperAdmin can log-in in the database

![server_maintenance_admin_login.png](https://bitbucket.org/repo/GRqnk7/images/2544647364-server_maintenance_admin_login.png)

![admin_login_maintenance.png](https://bitbucket.org/repo/GRqnk7/images/2916096262-admin_login_maintenance.png)

* Databases which are not in Maintenance mode will work normally.

![server_maintenance_db_without.png](https://bitbucket.org/repo/GRqnk7/images/2055055651-server_maintenance_db_without.png)

Disable Maintenance Mode:
-------------------------

Settings -> Configuration -> General Settings -> Options -> **Active Maintenance Mode** ``Uncheck`` option.
Then Click on ``Apply``

![active_false.png](https://bitbucket.org/repo/GRqnk7/images/968310793-active_false.png)