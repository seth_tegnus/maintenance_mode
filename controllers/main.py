# -*- coding: utf-8 -*-
#/#############################################################################
#
#    BizzAppDev
#    Copyright (C) 2004-TODAY bizzappdev(<http://www.bizzappdev.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#/#############################################################################


from openerp.addons.web import http
from openerp.tools import config
import openerp
import re
openerpweb = http

def db_list(req, force=False):
    proxy = req.session.proxy("db")
    dbs = proxy.list(force)
    h = req.httprequest.environ['HTTP_HOST'].split(':')[0]
    d = h.split('.')[0]
    r = openerp.tools.config['dbfilter'].replace('%h', h).replace('%d', d)
    dbs = [i for i in dbs if re.match(r, i)]
    return dbs
    
class maintenance(openerpweb.Controller):
    _cp_path = "/web/maintenance"

    @openerpweb.jsonrequest
    def get_maintenance(self, req, db=False):
        if not db:
            dbs = db_list(req)
            db = dbs and dbs[0] or False
        return config.get('maintenance', {}).get(db, False)
