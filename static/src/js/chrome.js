openerp.maintenance_mode = function(instance) {
    _t = instance.web._t;

    instance.auth_signup = instance.auth_signup || {};

    instance.web.Login.include({
        start: function() {
            var self = this;
            this.signup_enabled = false;
            this.reset_password_enabled = false;
            self = this;
            self.rpc("/web/maintenance/get_maintenance", {'db': this.params.db}).always(function (ret_val){
                if (ret_val){
                    self.on('change:login_mode', self, function() {
                        //self.set('login_mode', 'maintance');
                        //self.$('div.oe_login_pane:visible').toggle(false);
                        self.$el.addClass("oe_login_invalid");
                        self.$("div.oe_login_error_message").text(_t("Server is under Maintenance"));
                    });
                }
            })
            return this._super()
        },
        
        do_login: function (db, login, password) {
            var self = this;
            self.hide_error();
            self.$(".oe_login_pane").fadeOut("slow");
            return this.session.session_authenticate(db, login, password).then(function() {
                self.remember_last_used_database(db);
                if (self.has_local_storage && self.remember_credentials) {
                    localStorage.setItem(db + '|last_login', login);
                }
                self.trigger('login_successful');
                }, function () {
                    self.$(".oe_login_pane").fadeIn("fast", function() {
                        var msg = self.$("div.oe_login_error_message").text();
                        if (msg){
                            self.show_error(msg);
                        }
                        else{
                            self.show_error(_t("Invalid username or password"));
                        }
                    });
                });
        },
    })
}
