# -*- coding: utf-8 -*-
#/#############################################################################
#
#    BizzAppDev
#    Copyright (C) 2004-TODAY bizzappdev(<http://www.bizzappdev.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#/#############################################################################
{
    'name': 'OpenERP - Maintenance',
    'version': '7.0.0',
    "author": "Ruchir Shukla (Bizzappdev)",
    "website": "http://bizzappdev.com",
    "category": "GenericModules",
    'sequence': 20,
    'summary': 'OpenERP - Maintenance',
    'description': """
OpenERP - Maintenance
=====================

OpenERP / Odoo Module Which help you to put it under Maintenance mode.

Features:
---------

    * Enable Maintenance at level of database.
    * Login screen message.
    * Only SuperAdmin can login.
    * Already logged in user will get Message.


    """,
    'images': [],
    'depends': ["web", "auth_signup"],
    'data': [
        "res_config_view.xml",
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    'application': False,
    'js': ["static/src/js/chrome.js"],
    'qweb': [],
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
