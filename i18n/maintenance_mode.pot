# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* maintenance_mode
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 7.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-07-26 08:25+0000\n"
"PO-Revision-Date: 2014-07-26 08:25+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: maintenance_mode
#: model:ir.model,name:maintenance_mode.model_res_users
msgid "Users"
msgstr ""

#. module: maintenance_mode
#: code:addons/maintenance_mode/res_users.py:44
#: code:addons/maintenance_mode/res_users.py:51
#, python-format
msgid "Under Maintenance!"
msgstr ""

#. module: maintenance_mode
#: code:addons/maintenance_mode/res_users.py:44
#: code:addons/maintenance_mode/res_users.py:51
#, python-format
msgid "System is under Maintenance"
msgstr ""

#. module: maintenance_mode
#. openerp-web
#: code:addons/maintenance_mode/static/src/js/chrome.js:18
#, python-format
msgid "Server is under Maintenance"
msgstr ""

#. module: maintenance_mode
#. openerp-web
#: code:addons/maintenance_mode/static/src/js/chrome.js:45
#, python-format
msgid "Invalid username or password"
msgstr ""

#. module: maintenance_mode
#: field:base.config.settings,maintenance:0
msgid "Active Maintenance Mode"
msgstr ""

